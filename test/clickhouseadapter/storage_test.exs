defmodule Clickhouseadapter.Storage_test do
  use ExUnit.Case
  test "create db", %{} do
    # get status_code
    {:ok, resp} = Clickhouseadapter.Storage.storage_down(%{database: "testdatabasename"})
    assert resp.status_code == 200

  end

  test "drop db", %{} do
    {:ok, resp} = Clickhouseadapter.Storage.storage_down(%{database: "testdatabasename"})
    assert resp.status_code == 200
  end

end
