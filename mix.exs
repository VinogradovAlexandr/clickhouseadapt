defmodule Clickhouseadapter.Mixfile do
  use Mix.Project

  def project do
    [
      app: :clickhouseadapter,
      version: "0.0.1",
      elixir: "~> 1.4",
      elixirc_paths: elixirc_paths(Mix.env),
      compilers: [:phoenix, :gettext] ++ Mix.compilers,
      start_permanent: Mix.env == :prod,
      build_embedded: Mix.env == :prod,
      deps: deps()
    ]
  end

  # Configuration for the OTP application.
  #
  # Type `mix help compile.app` for more information.
  def application do
    [
      mod: {Clickhouseadapter.Application, []},
      extra_applications: [:logger, :runtime_tools]
    ]
  end

  # Specifies which paths to compile per environment.
  defp elixirc_paths(:test), do: ["lib", "test/support"]
  defp elixirc_paths(_),     do: ["lib"]

  # Specifies your project dependencies.
  #
  # Type `mix help deps` for examples and options.
  defp deps do
    [
      {:phoenix, "~> 1.3.4"},
      {:phoenix_pubsub, "~> 1.0"},
      {:phoenix_html, "~> 2.10"},
      {:phoenix_live_reload, "~> 1.0", only: :dev},
      {:phoenix_ecto, "~> 3.2"},
      {:db_connection, "~> 2.0"},
      {:gettext, "~> 0.11"},
      {:cowboy, "~> 1.0"},
      {:httpoison, "~> 1.4"},
      {:plug_cowboy, "~> 1.0"},
      {:ex_doc, "~> 0.19", only: :dev},
      {:ecto, "~> 3.0.0-rc.0"},
      {:ecto_sql, "~> 3.0.0-rc.0"},
      {:clickhouseadapter, git: "https://VinogradovAlexandr@bitbucket.org/VinogradovAlexandr/clickhouseadapter.git"}

    ]
  end
end
