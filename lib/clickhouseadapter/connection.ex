defmodule Clickhouseadapter.Connection do
  use DBConnection

  defstruct conn_opts: [:scheme, :hostname, :port, :database,
                        :username, :password, :timeout], base_address:

  @type state :: %{
                     conn_opts: Keyword.t,
                     base_address: String.t()
                   }
  @type cursor :: any

  def connect(opts) do
    scheme = opts[:scheme] || :http
    hostname = opts[:hostname] || "localhost"
    port = opts[:port] || 8123
    database = opts[:database] || "default"
    username = opts[:username] || nil
    password = opts[:password] || nil
    timeout = 60000
    base_address = build_base_address(scheme, hostname, port)



    case Clickhouseadapter.HTTP.http_post_request("http://localhost:8123/?query=", "SELECT 1") do
      {:ok, resp} ->

        {
          :ok,
          %Clickhouseadapter.Connection{
            conn_opts: [
              scheme: scheme,
              hostname: hostname,
              port: port,
              database: database,
              username: username,
              password: password,
              timeout: timeout,
            ],
            base_address: base_address
          }
        }

      resp -> resp
    end
    # what should it return?
  end

  @spec disconnect(err :: Exception.t, state) :: :ok
  def disconnect(_err, _state) do
    :ok
  end

  def checkin(state) do
    {:ok, state}
  end

  def checkout(state) do
    {:ok, state}
  end

  def handle_begin(opts, state) do
    {:ok, %Clickhouseadapter.Result{}, state}
  end

  def handle_close(query, opts, state) do

    {:ok, %Clickhouseadapter.Result{},  state}
  end

  def handle_commit(opts, state) do

    {:ok, %Clickhouseadapter.Result{}, state}
  end

  def handle_rollback(opts, state) do

    {:ok, %Clickhouseadapter.Result{}, state}
  end

  def handle_errors({:error, reason}) do
    {:error, reason}
  end

  def handle_prepare(query, _, state) do
    {:ok, query, state}
  end

  def handle_execute(query, params, opts, state) do
    
    Clickhouseadapter.Query.execute_query(query, params, opts, state)

  end

  def ping(state) do

  end



  defp build_base_address(scheme, hostname, port) do
      "#{Atom.to_string(scheme)}://#{hostname}:#{port}/"
  end

end
