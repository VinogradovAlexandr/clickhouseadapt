defmodule Clickhouseadapter.Result do
  defstruct   [
    :command,
    :columns ,
    :rows ,
    :num_rows
  ]
end
