defmodule ClickhouseadapterWeb.PageController do
  use ClickhouseadapterWeb, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
